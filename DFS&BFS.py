#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 10 19:44:07 2017

@author: JishnuRenugopal
"""
import copy
import queue as queue

class Graph(object):

    def __init__(self, graph_dict=None):
        """ initializes a graph object 
            If no dictionary or None is given, 
            an empty dictionary will be used
        """
        if graph_dict == None:
            graph_dict = {}
        self.__graph_dict = graph_dict
        self.dictCopy = copy.deepcopy(self.__graph_dict)
    def vertices(self):
        """ returns the vertices of a graph """
        return list(self.__graph_dict.keys())

    def edges(self):
        """ returns the edges of a graph """
        print(self.__graph_dict)
        return self.__generate_edges()

    def add_vertex(self, vertex):
        """ If the vertex "vertex" is not in 
            self.__graph_dict, a key "vertex" with an empty
            list as a value is added to the dictionary. 
            Otherwise nothing has to be done. 
        """
        if vertex not in self.__graph_dict:
            self.__graph_dict[vertex] = []

    def add_edge(self, edge):
        """ assumes that edge is of type set, tuple or list; 
            between two vertices can be multiple edges! 
        """
        edge = set(edge)
        (vertex1, vertex2) = tuple(edge)
        if vertex1 in self.__graph_dict:
            self.__graph_dict[vertex1].append(vertex2)
        else:
            self.__graph_dict[vertex1] = [vertex2]

    def __generate_edges(self):
        """ A static method generating the edges of the 
            graph "graph". Edges are represented as sets 
            with one (a loop back to the vertex) or two 
            vertices 
        """
        edges = []
        for vertex in self.__graph_dict:
            for neighbour in self.__graph_dict[vertex]:
                if {neighbour, vertex} not in edges:
                    edges.append({vertex, neighbour})
        return edges

    def __str__(self):
        res = "vertices: "
        for k in self.__graph_dict:
            res += str(k) + " "
        res += "\nedges: "
        for edge in self.__generate_edges():
            res += str(edge) + " "
        return res
    
        
    def DFS(self, vertex, root):
        
        """ A method that conducts depth first search on the given graph
        """
        ret=""
        if len(self.dictCopy) is not 0:
            if root in self.dictCopy.keys():
                del self.dictCopy[root]
                ret+=root
            if root==vertex:
                print ("Vertex found")
            for v in self.__graph_dict[root]:
                if v in self.dictCopy.keys():
                    ret+=self.DFS(vertex, v)
            return ret
                
    def BFS(self, vertex, root):
        """ A method that conducts breadth first search on the given graph
        """
        ret = root
        q = queue.Queue(maxsize=0)
        q.put(root)
        while not q.empty():
            current = q.get()
            if current is vertex:
                print ("Vertex found")
            for v in self.__graph_dict[current]:
                if v not in ret:
                    ret+=v
                    q.put(v)
        return ret
        
   
    def reset(self):
        self.dictCopy = copy.deepcopy(self.__graph_dict)
        
        

if __name__ == "__main__":

    g = { "a" : ["d", "f"],
          "b" : ["c"],
          "c" : ["b", "c", "d", "e"],
          "d" : ["a", "c"],
          "e" : ["c"],
          "f" : ["a"]
        }

#A simple test case to test the functionality of DFS
    graph = Graph(g)
    st=""
    print(graph.BFS('j', 'a'))
    graph.reset()
    
    
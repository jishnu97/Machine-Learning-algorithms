#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  7 21:04:55 2017

@author: JishnuRenugopal
"""


print(__doc__)
from sklearn import datasets, linear_model
import matplotlib.pyplot as plot
import numpy as np
import random as rand
import sys

#Import data
# Load the diabetes dataset
diabetes = datasets.load_diabetes()
# Use only one feature
diabetes_X = diabetes.data[:, np.newaxis, 2]

# Split the data into training/testing sets
diabetes_X_train = diabetes_X[:-20]
diabetes_X_test = diabetes_X[-20:]

# Split the targets into training/testing sets
diabetes_y_train = diabetes.target[:-20]
diabetes_y_test = diabetes.target[-20:]


#MSE calculator
def MSECalculator(y, yHat):
    Sum = (y - yHat) ** 2
    MSE = np.mean(Sum)
    return MSE
#--------------------------------------------
#Run linear regression
reg = linear_model.LinearRegression()
reg.fit(diabetes_X_train, diabetes_y_train)
print("The coefficient is:", reg.coef_)
print("Intercept for regression is:", reg.intercept_)
y = reg.predict(diabetes_X_test)
yHat = diabetes_y_test
Sum = MSECalculator(y, yHat)
MSE = np.mean(Sum)
print("The mean squared error is:", MSE)



#------------------------------------------
#See how linear regression performs

#Plot
plot.scatter(diabetes_X_test, diabetes_y_test, color='black')
plot.plot(diabetes_X_test, reg.predict(diabetes_X_test), color='blue',
         linewidth=3)
plot.show()

#--------------------------------------------

#Gradient descent
def gradDescent(x, y, stepSize, threshold, maxIterations):
    m = rand.uniform(0,1)
    c = rand.uniform(0,1)
    iterations = 0
    yHat = m*x+c
    MSE = MSECalculator(y, yHat)
    MSE_new = -sys.maxsize-1
    #X and Y arrays to see the change in error during gradient descent
    errX=[]
    errY = []
    while iterations<=maxIterations and MSE-MSE_new>threshold:
        
        x = x.ravel()
        yHat = yHat.ravel()
        m = m - stepSize * ((2 / len(y)) * (sum((yHat - y) * x)))
        c = c - stepSize * ((2 / len(y)) * (sum(yHat - y)))
        yHat = m*x+c
         
        if iterations>2:
            MSE=MSE_new
            #Update errors
            errY.append(MSE)
            errX.append(iterations)
        MSE_new = MSECalculator(y,yHat)
        iterations +=1
    plot.scatter(x, y, color='black')
    plot.plot(x, m*x+c, color='red')
    plot.show()
    axes = plot.gca()
    axes.set_ylim([4000,6000])
    axes.set_xlim([0,30000])
    plot.plot(errX, errY, color='red')
    plot.show()
    print("Optimal intercept is:", c)
    print("Optimal slope is:", m)
        
gradDescent(diabetes_X_train,diabetes_y_train,0.01,0.0000004,2500000)
    

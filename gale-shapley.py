#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr  8 21:59:30 2017

@author: JishnuRenugopal
"""
import numpy as np
import random as rd

#A bidirectional dictionary used to maintain the pairings between men and women
class bidict(dict):
    def __init__(self, *args, **kwargs):
        super(bidict, self).__init__(*args, **kwargs)
        self.inverse = {}
        for key, value in self.items():
            self.inverse.setdefault(value,[]).append(key) 

    def __setitem__(self, key, value):
        super(bidict, self).__setitem__(key, value)
        self.inverse.setdefault(value,[]).append(key)        

    def __delitem__(self, key):
        self.inverse.setdefault(self[key],[]).remove(key)
        if self[key] in self.inverse and not self.inverse[self[key]]: 
            del self.inverse[self[key]]
        super(bidict, self).__delitem__(key)

#Starts the gayle-shapley algorithm
def startAlgo(numPeople):
    baseArr = list(range(0,numPeople))
    print("Preferences for women:")
    prefWomen = createPreferences(baseArr)
    print("Preferences for men:")
    prefMen = createPreferences(baseArr)
    propose(prefWomen, prefMen)
    
'''
Randomizes the base array to create preferences the number in [0,0] denotes the 
first preference for the first person. The number itself represents 
the member of the opposite sex
'''
def createPreferences(baseArr):
    out = []
    for i in range(0,len(baseArr)):
        out.append(shuffler(baseArr))
    print(np.matrix(out))
    return out
    
def shuffler(arr):
    b = arr[:] # make a copy of the keys
    rd.shuffle(b) # shuffle the copy
    return b # return the copy

def propose(prefWomen, prefMen):
    numPeople = len(prefMen)
    indices = [0]*numPeople
    prefs = bidict({})
    while len(prefs) is not numPeople:
        for i in range(0,numPeople):
            bestMatch = prefMen[i][indices[i]]
            
            if i not in prefs:
                if bestMatch in prefs.inverse: 
                    bestMatchPair = prefs.inverse[bestMatch][0]
                    if prefWomen[bestMatch].index(i)< prefWomen[bestMatch].index(bestMatchPair):
                        del prefs[prefs.inverse[bestMatch][0]]
                        prefs[i]=bestMatch
                else:
                    prefs[i]=bestMatch
                indices[i]+=1
    print("A stable pairing is given below:")
    print(prefs)
            
        
            
                    
        
        
people = input("Enter the number of people to be paired: ")    
startAlgo(int(people))

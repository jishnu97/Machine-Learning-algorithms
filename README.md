This repo consists of important ML algorithms implemented in Python or R.

**Algorithms:**

Gradient descent:

A popular machine learning algorithm used to minimize a function. This is especially useful when the function to be minimized is complex and the derivative is not trivial.

Gale-Shapley:

This algorithm provides a solution to the problem of finding a stable matching between two equally sized sets of elements given an ordering of preferences for each element.
